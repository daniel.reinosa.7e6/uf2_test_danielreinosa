/*
Daniel Reinosa
20/01/2023
Exercici 1
 */
import java.util.*

val data = intArrayOf(78, 99, 71, 81, 96, 66, 98, 97, 65, 100, 67, 66, 77, 79, 122, 122, 115, 109, 105, 130, 100, 109,
    108, 129, 130, 110, 101, 122, 124, 104, 116, 127, 122, 129, 120, 110, 116, 122, 107, 120, 127, 129, 104, 126, 100,
    120, 111, 124, 111, 103, 108, 118, 107, 107, 107, 113, 109, 114, 111, 129, 114, 115, 119, 132, 167, 157, 162, 142,
    141, 137, 148, 137, 155, 135, 149, 147, 156, 145, 165, 133, 155, 151, 143, 159, 138, 162, 155, 168, 132, 170, 166,
    145, 159, 143, 155, 142, 136, 163, 135, 130, 162, 152, 165, 163, 148, 147, 165, 157, 145, 153, 142, 133, 205, 201,
    176, 173, 192, 188, 174, 203, 184, 208, 204, 186, 196, 208, 156, 160, 131, 169, 142, 138, 169, 146, 147, 130, 139,
    140, 144, 163, 150, 164, 131, 167, 153, 151, 130, 132, 163, 149, 132, 148, 135, 131, 154, 159, 161, 152, 157, 136,
    130, 149, 134, 160, 142, 168, 163, 154, 170, 170, 151, 161, 144, 136, 160, 100, 118, 121, 106, 114, 120, 116, 118,
    111, 115, 101, 114, 115, 118, 118, 108, 112, 129, 115, 125, 102, 110, 102, 111, 129, 104, 119, 111, 125, 111, 130,
    110, 105, 130, 105, 112, 111, 127, 122, 107, 112, 118, 106, 103, 103, 125, 116, 112, 117, 88, 85, 96, 84, 79, 91,
    70, 71, 76, 96, 77, 91, 66, 84)

fun main() {
    val scanner = Scanner(System.`in`)
    println("Indica una de las siguientes opciones \n")
    println("1.Mostrar todos los datos de lectura \n" +
            "2.Mostrar cantidad de medidas de la actividad \n" +
            "3.Mostrar frequencia cardíaca media \n" +
            "4.Mostrar frequencias cardíacas minima y maxima \n" +
            "5.Desplegar submenu de actividad \n" +
            "0.Salir de la aplicacion \n")
    val input = scanner.nextInt()

    when(input){
        0->exit()
        1-> printData()
        2->howmuch()
        3->mitjana()
        4->maxMin()
        5->subMenu()
    }


}
fun printData(){
    for (i in data){
        println(i)
    }
}
fun howmuch():Int {
    var count = 0
    for (i in data) {
        count++
    }
    return count
}
fun maxMin(){
    var min = 1000
    var max = 0
        for (i in data) {
            if (i< min)min = i
            if (i>max)max = i
        }
    println("Valor maxim:$max \n" +
            "Valor minim:$min")
}
fun mitjana(){
    var suma = 0
    for(i in data)suma += i
    println(suma/data.size)
}
fun subMenu(){
    val scanner = Scanner(System.`in`)
    println("Indica tu edad")
    var input = scanner.nextInt()
    val maximPuls = 220-input


}
fun exit(){
    kotlin.system.exitProcess(0)
}

