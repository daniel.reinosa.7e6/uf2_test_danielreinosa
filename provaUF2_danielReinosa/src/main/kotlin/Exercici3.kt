//definició dels colors que farem servir, agafant els backgrounds ANSI, i establint-los com a constants globals
const val ANSI_RESET = "\u001B[0m"
const val ANSI_BLACK = "\u001B[40m"
const val ANSI_RED = "\u001B[41m"
const val ANSI_GREEN = "\u001B[42m"
const val ANSI_YELLOW = "\u001B[43m"
const val ANSI_BLUE = "\u001B[44m"
const val ANSI_PURPLE = "\u001B[45m"
const val ANSI_CYAN = "\u001B[46m"
const val ANSI_WHITE = "\u001B[47m"

fun main(){
    var colors = arrayOf(ANSI_BLACK, ANSI_WHITE, ANSI_RED, ANSI_RED, ANSI_YELLOW, ANSI_BLUE, ANSI_PURPLE, ANSI_CYAN, ANSI_RESET, ANSI_GREEN)
    var color=1
    var intensitat=1
    var intensitatPujada=true

    print("Introdueix ordre: ")
    val instruccio= readln()
    do {
        when(instruccio) {
            if (instruccio == "TURN ON"){
                if(color==0){
                    color = ANSI_RESET
                    intensitat=1
                    intensitatPujada=true
                }
            }
            else if(instruccio =="TURN OFF") {
                if(color>0){
                    offColor=color
                    color=0
                    intensitat=0
                }
            }
            else if(){
                if (color<=colors.size-2) color++
                else if(color==colors.size-2) color=1
            }
            else if (instruccio == "INTENSITY"){
                if (intensitatPujada) intensitat++
                else if (intensitatPujada && intensitat==5){
                    intensitatPujada=false
                    intensitat--
                }
                else if(!intensitatPujada && intensitat>1)intensitat--
                else if(!intensitatPujada && intensitat==1)intensitat++

            }
        }
        println("Color: ${colors[color]}    $ANSI_BLUE - intensitat $intensitatPujada")
    } while(instruccio=="END")
}